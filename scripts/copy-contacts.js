import * as dotenv from 'dotenv';
import { readFile } from 'node:fs/promises';
import postgres from 'postgres';

dotenv.config();

async function main() {
  const users = JSON.parse(await readFile('./mock-users.json', 'utf-8'));
  const sql = postgres(process.env.POSTGRES_URL);
  await sql`insert into contacts ${sql(
    users,
    'first_name',
    'last_name',
    'email',
  )}`;
  await sql.end({});
}

main().catch(console.error);
