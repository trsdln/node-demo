import express from 'express';
import crypto from 'node:crypto';
import jwt from 'jsonwebtoken';
import { sql } from '../postgres.js';

export const publicUsersRouter = express.Router();
export const privateUsersRouter = express.Router();

function hashSha256(input) {
  return crypto.createHash('sha256').update(input).digest('hex');
}

function generateJwt(email) {
  return jwt.sign({ email }, process.env.JWT_SECRET, { expiresIn: '31d' });
}

publicUsersRouter.post('/signup', async (req, res) => {
  try {
    const user = req.body;
    const password_hash = hashSha256(user.password);

    const existingUsers =
      await sql`select _id from users where email = ${user.email}`;
    if (existingUsers.length > 0) {
      throw new Error('Email is already taken');
    }

    const userRecord = { password_hash, email: user.email };
    await sql`insert into users ${sql(userRecord, 'email', 'password_hash')}`;

    res.json({ token: generateJwt(userRecord.email) });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: err.toString() });
  }
});

publicUsersRouter.post('/login', async (req, res) => {
  try {
    const user = req.body;

    const existingUsers =
      await sql`select _id, email, password_hash from users where email = ${user.email}`;

    if (existingUsers.length == 0) {
      throw new Error('Incorrect login credentials');
    }

    const [targetUser] = existingUsers;

    const password_hash = hashSha256(user.password);

    if (password_hash !== targetUser.password_hash) {
      throw new Error('Incorrect login credentials');
    }

    res.json({ token: generateJwt(targetUser.email) });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: err.toString() });
  }
});

privateUsersRouter.get('/current-user', (req, res) => {
  res.json(req.currentUser);
});
