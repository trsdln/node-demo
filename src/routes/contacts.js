import express from 'express';
import { sql } from '../postgres.js';

export const contactsRouter = express.Router();

contactsRouter.get('/contacts', async (req, res) => {
  const { phrase } = req.query;
  const pattern = `%${phrase}%`;

  const filterByPhrase = sql`
    where first_name ilike ${pattern} or
      last_name ilike ${pattern} or
      email ilike ${pattern}
  `;

  const users = await sql`select * from contacts ${
    phrase ? filterByPhrase : sql``
  }`;

  res.json(users);
});
