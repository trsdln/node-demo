import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { contactsRouter } from './routes/contacts.js';
import { publicUsersRouter, privateUsersRouter } from './routes/users.js';
import { jwtAuthenticateMiddleware } from './authenticate.js';

const app = express();

const { ORIGIN_URL } = process.env;

app.use(cors({ origin: ORIGIN_URL }));
app.use(bodyParser.json());

app.use('/', publicUsersRouter);

app.use(jwtAuthenticateMiddleware);

app.use('/', privateUsersRouter);

app.use('/', contactsRouter);

const port = 4000;

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
