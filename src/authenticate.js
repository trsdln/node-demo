import jwt from 'jsonwebtoken';
import { sql } from './postgres.js';

export async function jwtAuthenticateMiddleware(req, res, next) {
  try {
    const authHeader = req.headers.authorization;
    if (authHeader) {
      const token = authHeader.split(' ')[1];
      const payload = jwt.verify(token, process.env.JWT_SECRET);

      const [user] =
        await sql`select _id, email from users where email = ${payload.email}`;
      req.currentUser = user;

      next();
    }
  } catch (err) {
    console.error(err);
    res.status(403).json({ error: 'Forbidden' });
  }
}
