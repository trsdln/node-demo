import postgres from 'postgres';

const { POSTGRES_URL } = process.env;

export const sql = postgres(POSTGRES_URL);
