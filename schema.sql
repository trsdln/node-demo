CREATE TABLE IF NOT EXISTS users (
    _id bigserial PRIMARY KEY,
    email varchar(40) UNIQUE NOT NULL,
    password_hash varchar(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS contacts (
    _id bigserial PRIMARY KEY,
    first_name varchar(40) NOT NULL,
    last_name varchar(100) NOT NULL,
    email varchar(100) NOT NULL
);
