## Getting Started

1. Install npm deps: `yarn install`

2. Setup DB and user: `psql postgres`

```sql
> CREATE DATABASE demodb;
> CREATE USER demoapp WITH superuser PASSWORD '12345';
```

3. Generate tables: `psql demodb<schema.sql`

4. Start backend app: `yarn start`
